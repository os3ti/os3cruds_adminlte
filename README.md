# cruds_adminlte
 dependencias:

 django-autocomplete-light==3.5.1

 django-crispy-forms==1.10.0

 opcional: django-parler==2.2

 instalação pip install -e "repositorio com ultima tag"

 Exemplo: pip install -e git+https://alexandreos3@bitbucket.org/os3ti/os3cruds_adminlte.git#egg=cruds-adminlte==1.40
## configuração

**Adicionar crispy config**
```
CRISPY_TEMPLATE_PACK = 'bootstrap4'
```
**1 criar arquivo no projeto**: cruds_config.py
Exemplo:
```
from cruds_adminlte.apps import DjangoCrudsAdminlteConfig

class CrudsConfig(DjangoCrudsAdminlteConfig):
    def menu(self, request):
        from cruds_adminlte.menu import Menu
        produto = [
            Menu('Produto',
                 'produtos_produto_list',
                 request),
        ]
        main = [
            Menu('Produtos',
                 'produtos_adicional_list',
                 request,
                 produto,
                 icon='fa fa-cubes')        
        ]

        return main        
```

**Adicionar no instaled apps com as demais dependências** 

```
    instaled_apps = [
        'dal',
        'dal_select2',
        'projeto.cruds_config.CrudsConfig',
    ]
```

**Sobreescrevendo base para colocar logo** 

Criar arquivo dentro de template /cruds/base_overwrite.html

```
{% extends "cruds/base.html" %}
{% load static %}
{% block stylesheets %}
    {{ block.super }}
    <link rel="stylesheet" type="text/css" href="{% static "css/custom-adminlte.css" %}?ver=6">
{% endblock %}
```

**Exemplo css**
<pre>
    .main-header .logo{
        background-image: url(img/logotipo.png );
        background-repeat: no-repeat;
        background-position: center center;
        background-size: 80% 85%;
    }
</pre>

**Usando metodo no list_fields** 

Colocar o seguinte código no models:
```
       def _categoria_hierarquia(self):
            return self.categoria.hierarquia()
        _categoria_hierarquia.short_description = 'Categoria'
```

**Registrando um admin**
```
from cruds_adminlte.crud import UserCRUDVIew, GroupCrudView
from cruds_adminlte import crud_admin


@crud_admin.register()
class GroupCrudView(GroupCrudView):
    pass


@crud_admin.register()
class UserCRUDVIew(UserCRUDVIew):
    pass


from cruds_adminlte.crud impotert CRUDView
from ..models import Adicional
from cruds_adminlte import crud_admin

@crud_admin.register()
class AdicionalCRUDView(CRUDView):
    search_fields = 'nome__icontains',
    model = Adicional
```

**Registro das urls**

```
    from cruds_adminlte import crud_admin
    path('admin/', include(crud_admin.site.urls)),
```

**Clerable Input file**

No Firefox quando existe imagem longa, os texto sobrepoem, para
resolver devemos usar um custom widget fornecedido pelo nosso admin!

```
from cruds_adminlte.fields import CrudsClearableFileInput
class BannerForm(forms.ModelForm):
    class Meta:
        model = Banner
        fields = '__all__'
        widgets = {
            'img': CrudsClearableFileInput,
            'img_mobile': CrudsClearableFileInput,
        }
```
**OUTROS WIDGETS**

```
from cruds_adminlte.widgets import DatePickerWidget
from cruds_adminlte.widgets import DateTimePickerWidget
from cruds_adminlte.widgets import ColorPickerWidget
from cruds_adminlte.widgets import TimePickerWidget

```

**Ignorando select2**

Caso queira que o select seja natural, ou seja, não se transforme
em um select2, coloque a class ignore-select2 no field



**USANDO COM PARLER**

```
from cruds_adminlte.crud.crud_view import ParlerCRUDView
from cruds_adminlte.forms import CrudsParlerModelForm

class BannerForm(CrudsParlerModelForm):
    class Meta:
        model = Banner
        fields = '__all__'


@crud_admin.register()
class BannerCRUDView(ParlerCRUDView):
    model = Banner
    add_form = BannerForm
    update_form = BannerForm
```

**HABILITANDO 2FA**

Para habilitar 2fa via email adicione a seguinte config no settings:

```
settings:

OS3CRUDS_ENABLE_2FA = True
MIDDLEWARE = [
    'cruds_adminlte.middleware.UpdateSessionAgeMiddleware',
]
    
```

Como cada projeto tem sua forma de enviar email a implementação do envio deve ficar no projeto,
para isso você precisa conectar no signal que envia o usuario e o código:

```
def send_2fa_email(sender, **kwargs):
    print(sender.user)
    print(sender.code)
    // implementa o envido de email passando o codigo no corpo do email

class ContaConfig(AppConfig):
    name = 'apps.conta'

    def ready(self):
        from cruds_adminlte.signals import send_2fa_code
        send_2fa_code.connect(send_2fa_email)
```