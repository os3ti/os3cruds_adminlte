from cruds_adminlte.crud import CRUDView
from django.forms import inlineformset_factory
from django.shortcuts import render

from ..crud import InlineAjaxCRUD


class CustomInlineAjaxCRUD(InlineAjaxCRUD):
    def get_create_view(self):
        CreateView = super().get_create_view()

        class MyCreateView(CreateView):
            def form_valid(self, form):
                ret = super().form_valid(form)
                form.save_m2m()
                return ret

        return MyCreateView


    def get_update_view(self):
        UpdateView = super().get_update_view()

        class MyUpdateView(UpdateView):
            def form_valid(self, form):
                ret = super().form_valid(form)
                form.save_m2m()
                return ret

        return MyUpdateView


class CRUDViewWithInlines(CRUDView):
    inlines_add_form = []
    inline_field = ''
    extra = 1

    def get_create_view(self):
        CreateView = super().get_create_view()

        class MyCreateView(CreateView):
            inlines_add_form = self.inlines_add_form
            parent = self.model
            inline_field = self.inline_field
            extra = self.extra
            # formsets = [] pensar em melhor no context data pra só chamar ele no render do else

            def form_valid(self, form):
                formsets = [
                    inlineformset_factory(self.parent, klass.Meta.model, form=klass, min_num=1)(
                        data=self.request.POST, files=self.request.FILES, instance=None, prefix=klass.Meta.prefix
                    )
                    for klass in self.inlines_add_form
                ]

                if all([formset.is_valid() for formset in formsets]):
                    object = form.save()
                    for formset in formsets:
                        for item in formset:
                            if list(item.cleaned_data.values()):
                                a = item.save(commit=False)
                                setattr(a, self.inline_field, object)
                                a.save()

                    object.save()  # para caso tenha um pré save q dependa dos objetos relacionados
                else:
                    context = self.get_context_data()
                    context['inlines'] = [{'title': formset.model._meta.verbose_name_plural, 'formset': formset} for formset in formsets]
                    return render(self.request, self.template_name, context)
                return super().form_valid(form)

            def get_context_data(self, **kwargs):
                context = super().get_context_data(**kwargs)
                context['inlines'] = self.get_inlines_create()
                return context

            def get_inlines_create(self):
                formsets = list()
                for klass in self.inlines_add_form:
                    _formset = inlineformset_factory(self.parent, klass.Meta.model, form=klass, extra=self.extra)
                    form = _formset(
                        instance=None,
                        prefix=klass.Meta.prefix,
                        data=self.request.POST if self.request.POST else None,
                        files=self.request.FILES if self.request.FILES else None,
                    )
                    formsets.append(form)

                return [{'title': formset.model._meta.verbose_name_plural, 'formset': formset} for formset in formsets]
        return MyCreateView
