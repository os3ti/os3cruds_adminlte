from django.views import View
from django.shortcuts import render, reverse
from django.contrib.auth import logout as django_logout, login as django_login
from django.http.response import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from ..forms import LoginForm, CodigoForm
try:
    from django.utils.translation import ugettext_lazy as _
except:
    from django.utils.translation import gettext_lazy as _
from random import randint
from ..models import TwoFactoAuth
from django.conf import settings
from uuid import uuid4
from ..signals import send_2fa_code


class LogoutView(LoginRequiredMixin, View):
    def get(self, request):
        django_logout(request)
        return HttpResponseRedirect(reverse('cruds_login'))


class LoginView(View):
    def post(self, request):
        form = LoginForm(request.POST)
        if not request.session.test_cookie_worked():#teste cookie
            form.add_error('usuario', _('Por favor habilite os cookies.'))
        elif form.is_valid():
            #finaliza test com cookie
            request.session.delete_test_cookie()
            retorno = 'next' in request.GET and request.GET['next'] or reverse('cruds_admin')
            if not getattr(settings, 'OS3CRUDS_ENABLE_2FA', False):
                django_login(request, form.user)
                request.session.set_expiry(0)
                return HttpResponseRedirect(retorno)
            else:
                TwoFactoAuth.objects.filter(user=form.user).delete()
                code = randint(100000, 999999)
                r = TwoFactoAuth.objects.create(
                    user=form.user,
                    code=code,
                    hash=uuid4())

                send_2fa_code.send(r)

                return HttpResponseRedirect('{}?next={}'.format(reverse('cruds_2fa', kwargs={'hash': r.hash}), retorno))

        return render(request, 'cruds/login.html', locals())

    def get(self, request):
        form = LoginForm()
        #habilita teste de cookie
        request.session.set_test_cookie()
        return render(request, 'cruds/login.html', locals())


class TwoFaView(View):
    def post(self, request, *args, **kwargs):
        form = CodigoForm(request.POST, hash=kwargs['hash'])
        retorno = 'next' in request.GET and request.GET['next'] or reverse('cruds_admin')
        if form.is_valid():
            django_login(request, form.user)
            request.session.set_expiry(60 * 60 * 24 * 7)
            return HttpResponseRedirect(retorno)
        return render(request, 'cruds/2fa.html', locals())

    def get(self, request, *args, **kwargs):
        form = CodigoForm()
        #habilita teste de cookie
        request.session.set_test_cookie()
        return render(request, 'cruds/2fa.html', locals())