from django.views.generic import TemplateView
from ..mixins import StaffRequired


class DashboardInicialView(StaffRequired, TemplateView):
    template_name = 'cruds/dashboard_inicial.html'