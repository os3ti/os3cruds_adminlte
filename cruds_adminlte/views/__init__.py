from .inlines import CRUDViewWithInlines, CustomInlineAjaxCRUD
from .dashboard_inicial import DashboardInicialView
from .login import LoginView, LogoutView, TwoFaView
from .change_password import AdminTrocarSenhaView