from django.views import generic
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from ..mixins import SuperUserRequiredMixin
try:
    from django.utils.translation import ugettext_lazy as _
except:
    from django.utils.translation import gettext_lazy as _



class AdminTrocarSenhaView(SuperUserRequiredMixin, SuccessMessageMixin, generic.FormView):
    form_class = AdminPasswordChangeForm
    template_name = 'cruds/change_password.html'
    success_message = _('Senha alterada com sucesso.')

    def dispatch(self, request, *args, **kwargs):
        self.user = get_object_or_404(get_user_model(), pk=self.kwargs.get('pk', -1))
        self.back = request.GET.get('back', '')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return super().get_context_data(user=self.user)

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        kw.update({'user': self.user})
        return kw

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return self.back