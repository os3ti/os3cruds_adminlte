from django import forms


class CurrencyField(forms.DecimalField):
    def __init__(self, *args, **kwargs):
        if 'widget' not in kwargs:
            kwargs['widget'] = forms.TextInput(attrs={"data-number": "yes"})
        super().__init__(*args, **kwargs)
        self.localize = True



