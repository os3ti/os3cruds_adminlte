from django import forms


class TwoSideField(forms.SelectMultiple):

    template_name = 'cruds/fields/twosides.html'

    def __init__(self, attrs=None, choices=()):
        _attrs = {
            'size': 8
        }

        if not attrs:
            attrs = dict()
        _attrs.update(attrs)

        super().__init__(_attrs, choices)

    def use_required_attribute(self, initial):
        # atributo required no HTML impede o form de dar submit
        # a validação do form continua checando se é requerido pois esta função só altera a renderização
        return False
