from django import forms


class CrudsClearableFileInput(forms.ClearableFileInput):
    template_name = 'cruds/widgets/crudsclearablefileinput.html'
