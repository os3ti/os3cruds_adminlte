"""
Middleware
"""
from __future__ import unicode_literals

from .shortcuts import render_to_json
from django.utils.deprecation import MiddlewareMixin
from ..utils import is_ajax


class AJAXMiddleware(MiddlewareMixin):
    """
    AJAX Middleware that decides when to convert the response to JSON.
    """

    def process_response(self, request, response):
        """
        If the request was made by AJAX then convert response to JSON,
        otherwise return the original response.
        """
        try:
            ajax = request.is_ajax()
        except:
            ajax = is_ajax(request)

        if ajax:
            return render_to_json(response)

        return response

    def process_exception(self, request, exception):
        """
        Catch exception if the request was made by AJAX,
        after will become up on JSON.
        """
        try:
            ajax = request.is_ajax()
        except:
            ajax = is_ajax(request)

        if ajax:
            return exception

