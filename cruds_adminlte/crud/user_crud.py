from ..forms import GroupForm, UserBaseForm, UserUpdateForm
from django.contrib.auth.models import Group
from cruds_adminlte.crud import CRUDView
from django.contrib.auth import get_user_model


class UserCRUDVIew(CRUDView):
    add_form = UserBaseForm
    update_form = UserUpdateForm
    model = get_user_model()
    list_fields = 'email', 'first_name', 'last_name', 'is_staff', 'is_active', 'is_superuser'
    search_fields = ['first_name__icontains', 'last_name__icontains', 'email__icontains']
    fields = 'email', 'first_name', 'last_name', 'is_active', 'is_superuser'

    def get_update_view(self):
        UpdateView = super().get_update_view()

        class MyUpdateView(UpdateView):
            def get_form_kwargs(self):
                kwargs = super().get_form_kwargs()
                kwargs['request'] = self.request
                return kwargs
        return MyUpdateView


class GroupCrudView(CRUDView):
    add_form = GroupForm
    update_form = GroupForm
    model = Group
    list_fields = ['id', 'name']
    search_fields = ['name__icontains']
    split_space_search = ' '
