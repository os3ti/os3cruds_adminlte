# encoding: utf-8

try: 
    from django.conf.urls import url, include
except: 
    from django.urls import re_path as url, include 
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.urls.base import reverse_lazy, reverse
from django.views.generic import (ListView, CreateView, DeleteView,
                                  UpdateView, DetailView)
from cruds_adminlte import utils
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
try:
    from django.utils.translation import ugettext_lazy as _
except:
    from django.utils.translation import gettext_lazy as _

from django.db.models.query_utils import Q
from cruds_adminlte.filter import get_filters
from .crud_mixin import CRUDMixin, ParlerCRUDMixin
from django.contrib import messages

class CRUDView(object):
    """
        CRUDView is a generic way to provide create, list, detail, update,
        delete views in one class,
        you can inherit for it and manage login_required, model perms,
        pagination, update and add forms
        how to use:

        In views

        .. code:: python

            from testapp.models import Customer
            from cruds_adminlte.crud import CRUDView
            class Myclass(CRUDView):
                model = Customer

        In urls.py

        .. code:: python
            myview = Myclass()
            urlpatterns = [
                url('path', include(myview.get_urls()))  # also support
                                                         # namespace
            ]

        The default behavior is check_login = True and check_perms=True but
        you can turn off with

        .. code:: python
            from testapp.models import Customer
            from cruds_adminlte.crud import CRUDView

            class Myclass(CRUDView):
                model = Customer
                check_login = False
                check_perms = False

        You also can defined extra perms with

        .. code:: python

            class Myclass(CRUDView):
                model = Customer
                perms = { 'create': ['applabel.mycustom_perm'],
                          'list': [],
                          'delete': [],
                          'update': [],
                          'detail': []
                        }
        If check_perms = True we will add default django model perms
         (<applabel>.[add|change|delete|view]_<model>)

        You can also overwrite add and update forms

        .. code:: python

            class Myclass(CRUDView):
                model = Customer
                add_form = MyFormClass
                update_form = MyFormClass

        And of course overwrite base template name

        .. code:: python

            class Myclass(CRUDView):
                model = Customer
                template_name_base = "mybase"

        Remember basename is generated like app_label/modelname if
        template_name_base is set as None and
        'cruds' by default so template loader search this structure

        basename + '/create.html'
        basename + '/detail.html'
        basename + '/update.html'
        basename + '/list.html'
        basename + '/delete.html'
        Note: also import <applabel>/<model>/<basename>/<view type>.html

        Using namespace

        In views

        .. code:: python

            from testapp.models import Customer
            from cruds_adminlte.crud import CRUDView
            class Myclass(CRUDView):
                model = Customer
                namespace = "mynamespace"

        In urls.py

        .. code:: python

            myview = Myclass()
            urlpatterns = [
                url('path', include(myview.get_urls(),
                                    namespace="mynamespace"))
            ]

        If you want to filter views add views_available list

        .. code:: python
            class Myclass(CRUDView):
                model = Customer
                views_available = ['create', 'list', 'delete',
                                   'update', 'detail']

    """

    model = None
    template_name_base = "cruds"
    template_blocks = {}
    namespace = None
    fields = '__all__'
    urlprefix = ""
    check_login = True
    check_perms = True
    paginate_by = 10
    paginate_template = 'cruds/pagination/prev_next.html'
    paginate_position = 'Bottom'
    update_form = None
    add_form = None
    display_fields = None
    list_fields = None
    inlines = None
    views_available = None
    template_father = "cruds/base_overwrite.html"
    search_fields = None
    split_space_search = False
    related_fields = None
    list_filter = None
    mixin = CRUDMixin

    """
    It's obligatory this structure
        perms = {
        'create': [],
        'list': [],
        'delete': [],
        'update': [],
        'detail': []
        }
    """
    perms = None

    #  DECORATORS

    def check_decorator(self, viewclass):
        if self.check_login:
            return login_required(viewclass, login_url='cruds_login')
        return viewclass

    def decorator_create(self, viewclass):
        return self.check_decorator(viewclass)

    def decorator_detail(self, viewclass):
        return self.check_decorator(viewclass)

    def decorator_list(self, viewclass):
        return self.check_decorator(viewclass)

    def decorator_update(self, viewclass):
        return self.check_decorator(viewclass)

    def decorator_delete(self, viewclass):
        return self.check_decorator(viewclass)

    #  GET GENERIC CLASS

    def get_create_view_class(self):
        return CreateView

    def get_create_view(self):
        CreateViewClass = self.get_create_view_class()

        class OCreateView(self.mixin, CreateViewClass):
            namespace = self.namespace
            perms = self.perms['create']
            all_perms = self.perms
            form_class = self.add_form
            view_type = 'create'
            views_available = self.views_available[:]
            check_perms = self.check_perms
            template_father = self.template_father
            template_blocks = self.template_blocks
            related_fields = self.related_fields
            inlines = self.inlines
            urlprefix = self.urlprefix

            def form_valid(self, form):
                messages.add_message(self.request, messages.SUCCESS, _('Cadastro realizado com sucesso.'))
                if not self.related_fields:
                    return super(OCreateView, self).form_valid(form)

                self.object = form.save(commit=False)
                for key, value in self.context_rel.items():
                    setattr(self.object, key, value)
                self.object.save()
                form.save_m2m()
                return HttpResponseRedirect(self.get_success_url())

            def get_success_url(self):
                url = utils.crud_url_name(
                    self.model, 'update', prefix=self.urlprefix)
                if self.namespace:
                    url = self.namespace + ":" + url
                url = reverse(url, kwargs={'pk': self.object.pk})
                if 'change_list' in self.request.GET:
                    url += '?' + self.request.GET.urlencode()
                return url

        return OCreateView

    def get_detail_view_class(self):
        return DetailView

    def get_detail_view(self):
        ODetailViewClass = self.get_detail_view_class()

        class ODetailView(self.mixin, ODetailViewClass):
            namespace = self.namespace
            perms = self.perms['detail']
            all_perms = self.perms
            view_type = 'detail'
            display_fields = self.display_fields
            inlines = self.inlines
            views_available = self.views_available[:]
            check_perms = self.check_perms
            template_father = self.template_father
            template_blocks = self.template_blocks
            related_fields = self.related_fields

            def get_success_url(self):
                url = super(ODetailView, self).get_success_url()
                if 'change_list' in self.request.GET:
                    url += '?' + self.request.GET.urlencode()
                return url

        return ODetailView

    def get_update_view_class(self):
        return UpdateView

    def get_update_view(self):
        EditViewClass = self.get_update_view_class()

        class OEditView(self.mixin, EditViewClass):
            namespace = self.namespace
            perms = self.perms['update']
            form_class = self.update_form
            all_perms = self.perms
            view_type = 'update'
            inlines = self.inlines
            views_available = self.views_available[:]
            check_perms = self.check_perms
            template_father = self.template_father
            template_blocks = self.template_blocks
            related_fields = self.related_fields

            def form_valid(self, form):
                if not self.related_fields:
                    return super(OEditView, self).form_valid(form)

                self.object = form.save(commit=False)
                for key, value in self.context_rel.items():
                    setattr(self.object, key, value)
                self.object.save()
                form.save_m2m()
                return HttpResponseRedirect(self.get_success_url())

            def get_success_url(self):
                url = super(OEditView, self).get_success_url()
                if 'change_list' in self.request.GET:
                    url += '?' + self.request.GET.urlencode()
                return url

        return OEditView

    def get_list_view_class(self):
        return ListView

    def get_list_view(self):
        OListViewClass = self.get_list_view_class()

        class OListView(self.mixin, OListViewClass):
            namespace = self.namespace
            perms = self.perms['list']
            all_perms = self.perms
            list_fields = self.list_fields
            view_type = 'list'
            paginate_by = self.paginate_by
            views_available = self.views_available[:]
            check_perms = self.check_perms
            template_father = self.template_father
            template_blocks = self.template_blocks
            search_fields = self.search_fields
            split_space_search = self.split_space_search
            related_fields = self.related_fields
            paginate_template = self.paginate_template
            paginate_position = self.paginate_position
            list_filter = self.list_filter

            def get_listfilter_queryset(self, queryset):
                if self.list_filter:
                    filters = get_filters(
                        self.model, self.list_filter, self.request)
                    for filter in filters:
                        queryset = filter.get_filter(queryset)

                return queryset

            def search_queryset(self, query):
                if self.split_space_search is True:
                    self.split_space_search = ' '

                if self.search_fields and 'q' in self.request.GET:
                    q = self.request.GET.get('q')
                    if self.split_space_search:
                        q = q.split(self.split_space_search)
                    elif q:
                        q = [q]
                    sfilter = None
                    for field in self.search_fields:
                        for qsearch in q:
                            if field not in self.context_rel:
                                if sfilter is None:
                                    sfilter = Q(**{field: qsearch})
                                else:
                                    sfilter |= Q(**{field: qsearch})
                    if sfilter is not None:
                        query = query.filter(sfilter)

                if self.related_fields:
                    query = query.filter(**self.context_rel)
                return query

            def get_success_url(self):
                url = super(OListView, self).get_success_url()
                if (self.getparams):  # fixed filter detail action
                    url += '?' + self.getparams
                return url

            def get_queryset(self):
                queryset = super(OListView, self).get_queryset()
                queryset = self.search_queryset(queryset)
                queryset = self.get_listfilter_queryset(queryset)
                return queryset

        return OListView

    def get_delete_view_class(self):
        return DeleteView

    def get_delete_view(self):
        ODeleteClass = self.get_delete_view_class()

        class ODeleteView(self.mixin, ODeleteClass):
            namespace = self.namespace
            perms = self.perms['delete']
            all_perms = self.perms
            view_type = 'delete'
            views_available = self.views_available[:]
            check_perms = self.check_perms
            template_father = self.template_father
            template_blocks = self.template_blocks
            related_fields = self.related_fields

            def get_context_data(self, **kwargs):
                context = super().get_context_data(**kwargs)
                from django.contrib.admin.utils import get_deleted_objects
                from django.contrib import admin
                deleted_objects, model_count, perms_needed, protected = get_deleted_objects([self.object],
                                                                                            self.request,
                                                                                            admin.site)
                context['deleted_objects'] = deleted_objects
                return context

            def get_success_url(self):
                url = super(ODeleteView, self).get_success_url()
                if 'change_list' in self.request.GET:
                    url += '?' + self.request.GET.urlencode()
                return url

        return ODeleteView

#  INITIALIZERS
    def initialize_create(self, basename):
        OCreateView = self.get_create_view()
        url = utils.crud_url_name(
            self.model, 'list', prefix=self.urlprefix)
        if self.namespace:
            url = self.namespace + ":" + url

        fields = self.fields
        if self.add_form:
            fields = None
        self.create = self.decorator_create(OCreateView.as_view(
            model=self.model,
            fields=fields,
            success_url=reverse_lazy(url),
            template_name=basename
        ))

    def initialize_detail(self, basename):
        ODetailView = self.get_detail_view()
        self.detail = self.decorator_detail(
            ODetailView.as_view(
                model=self.model,
                template_name=basename
            ))

    def initialize_update(self, basename):
        OUpdateView = self.get_update_view()
        url = utils.crud_url_name(
            self.model, 'list', prefix=self.urlprefix)
        if self.namespace:
            url = self.namespace + ":" + url
        fields = self.fields
        if self.update_form:
            fields = None
        self.update = self.decorator_update(OUpdateView.as_view(
            model=self.model,
            fields=fields,
            success_url=reverse_lazy(url),
            template_name=basename
        ))

    def initialize_list(self, basename):
        OListView = self.get_list_view()
        self.list = self.decorator_list(OListView.as_view(
            model=self.model,
            template_name=basename
        ))

    def initialize_delete(self, basename):
        ODeleteView = self.get_delete_view()
        url = utils.crud_url_name(
            self.model, 'list', prefix=self.urlprefix)
        if self.namespace:
            url = self.namespace + ":" + url
        self.delete = self.decorator_delete(ODeleteView.as_view(
            model=self.model,
            success_url=reverse_lazy(url),
            template_name=basename
        ))

    def get_base_name(self):
        ns = self.template_name_base
        if not self.template_name_base:
            ns = "%s/%s" % (
                self.model._meta.app_label,
                self.model.__name__.lower())
        return ns

    def check_create_perm(self, applabel, name):
        notfollow = False
        try:
            model, created = ContentType.objects.get_or_create(
                app_label=applabel, model=name)
        except:
            notfollow = True
        if not notfollow and not Permission.objects.filter(content_type=model,
                                                           codename="view_%s" %
                                                           (name, )).exists():
            Permission.objects.create(
                content_type=model,
                codename="view_%s" % (name,),
                name=_("Can see available %s" % (name,)))

    def initialize_perms(self):
        if self.perms is None:
            self.perms = {
                'create': [],
                'list': [],
                'delete': [],
                'update': [],
                'detail': []

            }
        applabel = self.model._meta.app_label
        name = self.model.__name__.lower()
        if self.check_perms:
            self.check_create_perm(applabel, name)
            self.perms['create'].append("%s.add_%s" % (applabel, name))
            self.perms['update'].append("%s.change_%s" % (applabel, name))
            self.perms['delete'].append("%s.delete_%s" % (applabel, name))
            # maybe other default perm can be here
            self.perms['list'].append("%s.view_%s" % (applabel, name))
            self.perms['detail'].append("%s.view_%s" % (applabel, name))

    def initialize_views_available(self):
        if self.views_available is None:
            self.views_available = [
                'create', 'list', 'delete', 'update', 'detail']

    def __init__(
        self, namespace=None, model=None, template_name_base=None,
        mixin=None
    ):
        if namespace:
            self.namespace = namespace
        if model:
            self.model = model
        if template_name_base:
            self.template_name_base = template_name_base
        if mixin:
            self.mixin = mixin

        basename = self.get_base_name()
        self.initialize_views_available()
        self.initialize_perms()
        if 'create' in self.views_available:
            self.initialize_create(basename + '/create.html')

        if 'detail' in self.views_available:
            self.initialize_detail(basename + '/detail.html')

        if 'update' in self.views_available:
            self.initialize_update(basename + '/update.html')

        if 'list' in self.views_available:
            self.initialize_list(basename + '/list.html')

        if 'delete' in self.views_available:
            self.initialize_delete(basename + '/delete.html')

    def get_urls(self):

        pre = ""
        try:
            if self.cruds_url:
                pre = "%s/" % self.cruds_url
        except AttributeError:
            pre = ""
        base_name = "%s%s/%s" % (pre, self.model._meta.app_label,
                                 self.model.__name__.lower())
        myurls = []
        if 'list' in self.views_available:
            myurls.append(url("^%s/list$" % (base_name,),
                              self.list,
                              name=utils.crud_url_name(
                                  self.model, 'list', prefix=self.urlprefix)))
        if 'create' in self.views_available:
            myurls.append(url("^%s/create$" % (base_name,),
                              self.create,
                              name=utils.crud_url_name(
                                  self.model, 'create', prefix=self.urlprefix))
                          )
        if 'detail' in self.views_available:
            myurls.append(url('^%s/(?P<pk>[^/]+)$' % (base_name,),
                              self.detail,
                              name=utils.crud_url_name(
                                  self.model, 'detail', prefix=self.urlprefix))
                          )
        if 'update' in self.views_available:
            myurls.append(url("^%s/(?P<pk>[^/]+)/update$" % (base_name,),
                              self.update,
                              name=utils.crud_url_name(
                                  self.model, 'update', prefix=self.urlprefix))
                          )
        if 'delete' in self.views_available:
            myurls.append(url(r"^%s/(?P<pk>[^/]+)/delete$" % (base_name,),
                              self.delete,
                              name=utils.crud_url_name(
                                  self.model, 'delete', prefix=self.urlprefix))
                          )

        myurls += self.add_inlines(base_name)
        return myurls

    def add_inlines(self, base_name):
        dev = []
        if self.inlines:
            for i, inline in enumerate(self.inlines):
                klass = inline
                if isinstance(klass, type):
                    # FIXME: This is a dirty hack to act on repeated calls to get_urls()
                    #        as those mean that inline is a type instance not a class from
                    #        the second run onwars.
                    klass = klass()
                self.inlines[i] = klass
                if self.namespace:
                    dev.append(
                        url('^inline/',
                            include(klass.get_urls(),
                                    namespace=self.namespace))
                    )
                else:
                    dev.append(
                        url('^inline/', include(klass.get_urls()))

                    )
        return dev


class ParlerCRUDView(CRUDView):
    mixin = ParlerCRUDMixin

    def get_update_view(self):
        EditViewClass = super().get_update_view()

        class PalertEditView(EditViewClass):
            urlprefix = self.urlprefix
            def get_object(self, queryset=None):
                object = super().get_object(queryset)
                if object is not None:  # Allow fallback to regular models.
                    object.set_current_language(self._language(self.request), initialize=True)
                return object

            def get_form_kwargs(self):
                k = super().get_form_kwargs()
                k['__language_code'] = self._language(self.request)
                return k

            def get_success_url(self):
                url = utils.crud_url_name(
                    self.model, 'update', prefix=self.urlprefix)
                if self.namespace:
                    url = self.namespace + ":" + url
                url = reverse(url, kwargs={'pk': self.object.pk})

                if 'change_list' in self.request.GET:
                    url += '?' + self.request.GET.urlencode()

                return url

            def form_valid(self, form):
                messages.add_message(self.request, messages.SUCCESS, _('Dados atualizados com sucesso.'))
                return super(PalertEditView, self).form_valid(form)

        return PalertEditView

    def get_create_view(self):
        CreateViewClass = super().get_create_view()

        class ParlerCreateView(CreateViewClass):
            def get_form_kwargs(self):
                k = super().get_form_kwargs()
                k['__language_code'] = self._language(self.request)
                return k

        return ParlerCreateView

    def get_list_view(self):
        ListViewClass = super().get_list_view()

        class ParlerListView(ListViewClass):
            def get_queryset(self):
                queryset = super().get_queryset().distinct()
                # if hasattr(queryset, 'active_translations'):
                #     queryset = queryset.active_translations()
                return queryset
        return ParlerListView
