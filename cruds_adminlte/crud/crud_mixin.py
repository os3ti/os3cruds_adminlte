# encoding: utf-8
from django.http.response import HttpResponseForbidden
from django.urls.base import reverse
from django.urls.exceptions import NoReverseMatch
from django.views import View
from cruds_adminlte import utils
from django.shortcuts import get_object_or_404
from cruds_adminlte.filter import get_filters
from django.template.loader import render_to_string
import types


class CRUDMixin(object):

    def get_template_names(self):
        dev = []
        base_name = "%s/%s/" % (self.model._meta.app_label,
                                self.model.__name__.lower())
        dev.append(base_name + self.template_name)
        dev.append(self.template_name)
        base = self.template_name.split("/")[-1]
        dev.append("cruds/" + base)

        return dev

    def get_search_fields(self, context):
        try:
            context['search'] = self.search_fields
        except AttributeError:
            context['search'] = False
        if self.view_type == 'list' and 'q' in self.request.GET:
            context['q'] = self.request.GET.get('q', '')

    def get_filters(self, context):
        filter_params = []
        if self.view_type == 'list' and self.list_filter:
            filters = get_filters(self.model, self.list_filter, self.request)
            context['filters'] = filters
            for filter in filters:
                param = filter.get_params(self.related_fields or [])
                if param:
                    filter_params += param

        if filter_params:
            if self.getparams:
                self.getparams += "&"
            self.getparams += "&".join(filter_params)

    def validate_user_perms(self, user, perm, view):
        if isinstance(perm, types.FunctionType):
            return perm(user, view)
        return user.has_perm(perm)

    def get_check_perms(self, context):
        user = self.request.user
        available_perms = {}
        for perm in self.all_perms:
            if self.check_perms:
                if perm in self.views_available:
                    available_perms[perm] = all([
                        self.validate_user_perms(user, x, perm)
                        for x in self.all_perms[perm]])
                else:
                    available_perms[perm] = False
            else:
                available_perms[perm] = True
        context['crud_perms'] = available_perms

    def get_urls_and_fields(self, context):
        include = None
        if hasattr(self, 'display_fields') and self.view_type == 'detail':
            include = getattr(self, 'display_fields')

        if hasattr(self, 'list_fields') and self.view_type == 'list':
            include = getattr(self, 'list_fields')

        context['fields'] = utils.get_fields(self.model, include=include)
        if hasattr(self, 'object') and self.object:
            for action in utils.INSTANCE_ACTIONS:
                try:
                    nurl = utils.crud_url_name(self.model, action)
                    if self.namespace:
                        nurl = self.namespace + ':' + nurl
                    url = reverse(nurl, kwargs={'pk': self.object.pk})
                except NoReverseMatch:
                    url = None
                context['url_%s' % action] = url

        for action in utils.LIST_ACTIONS:
            try:
                nurl = utils.crud_url_name(self.model, action)
                if self.namespace:
                    nurl = self.namespace + ':' + nurl
                url = reverse(nurl)
            except NoReverseMatch:
                url = None
            context['url_%s' % action] = url

    def get_context_data(self, **kwargs):
        """
        Adds available urls and names.
        """

        context = super(CRUDMixin, self).get_context_data(**kwargs)
        context.update({
            'model_verbose_name': self.model._meta.verbose_name,
            'model_verbose_name_plural': self.model._meta.verbose_name_plural,
            'namespace': self.namespace
        })
        context.update({'blocks': self.template_blocks})

        if self.view_type in ['update', 'detail']:
            context['inlines'] = self.inlines

        if 'object' not in context:
            context['object'] = self.model

        self.get_urls_and_fields(context)
        self.get_check_perms(context)
        self.get_search_fields(context)
        self.get_filters(context)

        context['views_available'] = self.views_available
        if self.view_type == 'list':
            context['paginate_template'] = self.paginate_template
            context['paginate_position'] = self.paginate_position

        context['template_father'] = self.template_father

        context.update(self.context_rel)
        context['getparams'] = "?" + self.getparams
        context['getparams'] += "&" if self.getparams else ""

        return context

    def dispatch(self, request, *args, **kwargs):
        self.related_fields = self.related_fields or []
        self.context_rel = {}
        getparams = []
        self.getparams = ''
        for related in self.related_fields:
            pk = self.request.GET.get(related, '')
            if pk:
                Classrelated = utils.get_related_class_field(
                    self.model, related)
                self.context_rel[related] = get_object_or_404(
                    Classrelated, pk=pk)
                getparams.append("%s=%s" % (
                    related, str(self.context_rel[related].pk)))

        if getparams:
            self.getparams = "&".join(getparams)
        for perm in self.perms:
            if not self.validate_user_perms(request.user, perm,
                                            self.view_type):
                return HttpResponseForbidden(render_to_string(
                    'cruds/403.html', request=request
                ))
        return View.dispatch(self, request, *args, **kwargs)


class ParlerCRUDMixin(CRUDMixin):
    query_language_key = 'language'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if hasattr(self, 'object'):
            available_languages = self.get_available_languages(self.object)
            language_tabs = self.get_language_tabs(self.request, self.object, available_languages)
            context['language_tabs'] = language_tabs
        return context

    def get_available_languages(self, obj):
        """
        Fetching the available languages as queryset.
        """
        if obj:
            return obj.get_available_languages()
        else:
            return self.model._parler_meta.root_model.objects.none()


    def get_language_tabs(self, request, obj, available_languages, css_class=None):
        from parler.utils.views import get_language_tabs
        """
        Determine the language tabs to show.
        """
        current_language = self.get_form_language(request, obj)
        return get_language_tabs(request, current_language, available_languages, css_class=css_class)

    def _language(self, request):
        from parler.utils.views import get_language_parameter
        """
        Get the language parameter from the current request.
        """
        return get_language_parameter(request, self.query_language_key)

    def get_form_language(self, request, obj=None):
        """
        Return the current language for the currently displayed object fields.
        """
        if obj is not None:
            return obj.get_current_language()
        else:
            return self._language(request)