from django import template
from django.apps import apps
register = template.Library()


@register.simple_tag()
def get_cruds_menu(request):
    config = apps.get_app_config('cruds_adminlte')
    return config.menu(request)


