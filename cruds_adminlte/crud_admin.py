from .crud import CRUDView
from django.urls import path, include
from .views import DashboardInicialView, LoginView, LogoutView, AdminTrocarSenhaView, TwoFaView


class CrudSite(object):
    def __init__(self):
        self._urls = dict()

    def register(self, cruds_class):
        model = cruds_class.model
        if model._meta.app_label not in self._urls:
            self._urls[model._meta.app_label] = list()

        self._urls[model._meta.app_label].extend(cruds_class().get_urls())

    @property
    def urls(self):
        l = [
            path('', DashboardInicialView.as_view(),
                 name='cruds_admin'),
            path('login', LoginView.as_view(),
                 name='cruds_login'),
            path('2fa/<slug:hash>', TwoFaView.as_view(),
                 name='cruds_2fa'),
            path('logout', LogoutView.as_view(),
                 name='cruds_logout'),
            path('change-password/<int:pk>/', AdminTrocarSenhaView.as_view(),
                 name='cruds_change_password'),
        ]
        for k, v in self._urls.items():
            l.append(path('', include(v)))
        return l


site = CrudSite()


def register():
    def _model_admin_wrapper(cruds_class):
        if not issubclass(cruds_class, CRUDView):
            raise ValueError('Wrapped class must subclass CRUDView.')

        site.register(cruds_class=cruds_class)

        return cruds_class
    return _model_admin_wrapper
