from django.conf import settings
from django.contrib.sites.models import Site
from django.utils.functional import cached_property
from django.urls import reverse, resolve
from urllib import parse


class Menu(object):
    def __init__(self, titulo, url_name, request, children=[], live_options=None, **kwargs):
        self.request = request
        self.titulo = titulo
        self.url_name = url_name
        self.children = children or []
        self.extra_url = kwargs.get('extra_url', [])
        self.img_url = kwargs.get('img_url', None)
        if 'permission' in kwargs:
            self.permission = kwargs.get('permission')
        else:
            if 'external_url' in kwargs:
                self.permission = True
            else:
                l = url_name.split('_')
                model = l[-2]
                app_label = url_name.replace('_{}_{}'.format(l[-2], l[-1]), '')
                self.permission = request.user.has_perm('{}.{}_{}'.format(app_label, 'view', model))
        self.get = kwargs.get('get', '')
        self.https = kwargs.get('https', False)
        self.external_url = kwargs.get('external_url', '')
        self.get = parse.quote(self.get) if self.get else False
        self.img_url = '%s%s' % (settings.STATIC_URL, self.img_url) if self.img_url else False
        self.icon = kwargs.get('icon', False)
        self.live_options = live_options or {}

    def is_active(self):
        url_1 = resolve(parse.urlparse(self.request.path)[2])
        if url_1.url_name == self.url_name:
            if 'url' in self.request.GET:
                if self.get and parse.quote(self.request.GET['url']) == self.get:
                    return True
            else:
                return True

        for c in self.children:
            if c.is_active():
                return True

        for c in self.extra_url:
            if url_1.url_name == c:
                return True

        return False
    
    def get_is_active_css_class(self):
        return 'active' if self.is_active() else ''

    def get_link(self):
        if self.external_url:
            return self.external_url
        if self.get:
            return u'%s?url=%s' % (reverse(self.url_name), self.get)
        if self.https:
            return u'https://%s%s' % (Site.objects.get_current().domain, reverse(self.url_name))
        return reverse(self.url_name)

    @cached_property
    def has_children(self):
        return len(self.children) > 0

    def has_permission(self):
        if self.children:
            ret = False
            for c in self.children:
                if c.permission:
                    ret = True
            return ret
        return self.permission
