from django.utils.safestring import mark_safe


class ActionsMixin:
    """
    Mixin usado em conjunto com o templatetag cruds_custom.website_tags.actions.

    Semelhante a funcionalidade do ModelAdmin de, na listagem, permitir renderizar o retorno de um método.

    Ex:
    class MyListView(ActionsMixin, ListView):
        actions = 'hellow',

        def action_hello(self, obj):
            return 'hello'

    """

    actions = ()

    @staticmethod
    def _button(css_class, value, url, attrs=None, is_ajax=False, is_modal=False):
        classes = ['btn', css_class]
        if is_ajax:
            classes.append('ajax-action')
        elif is_modal:
            classes.append('open-modal')
        classes = ' '.join(classes)

        additional_attrs = []
        for k, v in (attrs or {}).items():
            additional_attrs.append(f'{k}="{v}"')
        additional_attrs = ' '.join(additional_attrs)

        return mark_safe(f'<a href="{url}" class="{classes}" {additional_attrs}>{value}</a>')
