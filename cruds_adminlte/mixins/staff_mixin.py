from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.urls import reverse_lazy
try:
    from django.utils.translation import ugettext_lazy as _
except:
    from django.utils.translation import gettext_lazy as _



class CrudsAdminRequired(LoginRequiredMixin):
    login_url = reverse_lazy('cruds_login')


class StaffRequired(LoginRequiredMixin):
    login_url = reverse_lazy('cruds_login')

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and not request.user.is_staff:
            raise Http404
        return super().dispatch(request, *args, **kwargs)


class SuperUserRequiredMixin(LoginRequiredMixin):
    permission_denied_message = _("Acesso destinado apenas para superusuários.")

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
