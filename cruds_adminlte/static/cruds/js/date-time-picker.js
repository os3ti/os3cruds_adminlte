$(function () {
    $('.datepickerwidget').datepicker({
        autoclose: true,
        language: 'pt-BR'
    });

    $(".timepickerwidget").timepicker({
        showInputs: false,
        showMeridian: false,
        minuteStep: 5,
        defaultTime: false
    });
});
