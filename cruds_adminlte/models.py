# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
try:
    from django.utils.translation import ugettext_lazy as _
except:
    from django.utils.translation import gettext_lazy as _


class TwoFactoAuth(models.Model):
    class Meta:
        verbose_name = _('Recuperar Senha')
        verbose_name_plural = _('Recuperar Senha')
        ordering = ('pk',)

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)

    data = models.DateTimeField(auto_now=True)

    code = models.BigIntegerField()

    hash = models.UUIDField(null=True)

    def __str__(self):
        return str(self.pk)
