from django.contrib.auth.models import Group
from django.forms import ModelForm
from ..fields import TwoSideField


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = '__all__'
        widgets = {
            'permissions': TwoSideField()
        }