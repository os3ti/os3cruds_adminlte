from .login import LoginForm, CodigoForm
from .group import GroupForm
from .user import UserBaseForm, UserUpdateForm
from .parler import CrudsParlerModelForm