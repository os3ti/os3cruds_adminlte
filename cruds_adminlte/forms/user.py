from django import forms
from django.urls import reverse

from ..fields import TwoSideField
from django.contrib.auth import get_user_model
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Layout, Field, Row, Submit, HTML
try:
    from django.utils.translation import ugettext_lazy as _
except:
    from django.utils.translation import gettext_lazy as _



class UserBaseForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = 'first_name', 'last_name', 'email', 'is_superuser', 'is_active', \
                 'is_staff', 'user_permissions', 'groups'

        widgets = {
            'user_permissions': TwoSideField(),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_tag = True
        self.helper.layout = Layout()
        self.layout()

        btns = list()
        btns.append(Submit('submit', _('Salvar'), css_class='btn btn-primary'))
        if self.instance.pk:
            btns.append(HTML('<a class="btn btn-primary" href="{}?back={}">{}</a>'.format(\
                reverse('cruds_change_password',
                        kwargs={'pk': self.instance.pk}),
                self.request.path,
                _('Alterar Senha'))))
        self.helper.layout.append(
            FormActions(*btns)
        )

    def layout(self):
        rows = [Row(
                Field('email', wrapper_class="col-md-12"),
            ),
            Row(
                Field('first_name', wrapper_class="col-md-6"),
                Field('last_name', wrapper_class="col-md-6"),
            ),
            Row(
                Field('is_superuser', wrapper_class="col-md-12"),
            ),
            Row(
                Field('is_active', wrapper_class="col-md-12"),
            ),
            Row(
                Field('is_staff', wrapper_class="col-md-12"),
            ),

            Row(
                Field('groups', wrapper_class="col-md-12"),
            ),
            Row(
                Field('user_permissions', wrapper_class="col-md-12"),
            )
        ]
        self.helper.layout.fields.extend(rows)

    def save(self, commit=True):
        user = super().save(commit=False)
        if commit:
            user.save()
        self.save_m2m()
        return user


class UserUpdateForm(UserBaseForm):
    pass