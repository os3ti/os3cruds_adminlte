try:
    from parler.forms import TranslatableModelForm
except:
    from django.forms import ModelForm as TranslatableModelForm


class CrudsParlerModelForm(TranslatableModelForm):
    def __init__(self, *args, **kwargs):
        if '__language_code' in kwargs:
            self.language_code = kwargs['__language_code']
            del kwargs['__language_code']

        super().__init__(*args, **kwargs)

        for f in self._meta.model.translations.field.model._meta.get_fields():
            if f.name in self.fields:
                self.fields[f.name].label = '{} ({})'.format(self.fields[f.name].label, self.language_code)
