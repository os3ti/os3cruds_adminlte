# -*- coding: utf-8 -*-
from django.forms.utils import ErrorList
from django import forms
from django.contrib.auth import authenticate
try:
    from django.utils.translation import ugettext_lazy as _
except:
    from django.utils.translation import gettext_lazy as _
from ..models import TwoFactoAuth
from dateutil.relativedelta import relativedelta
from django.utils import timezone


class LoginForm(forms.Form):
    _user = None
    usuario = forms.CharField(label=_("Usuário"), widget=forms.TextInput(attrs={'tabindex': 1, 'class': 'form-control'}))
    senha = forms.CharField(label=_("Senha"), max_length=20, widget=forms.PasswordInput(attrs={'tabindex': 2, 'class': 'form-control'}))

    def clean(self):
        cleaned_data = self.cleaned_data
        if 'usuario' in cleaned_data and 'senha' in cleaned_data:
            self._user = authenticate(username=cleaned_data['usuario'], password=cleaned_data['senha'])
            if not self._user:
                self._errors['usuario'] = ErrorList([_(u'Usuário e/ou senha inválido.')])
        return cleaned_data

    @property
    def user(self):
        return self._user


class CodigoForm(forms.Form):
    codigo = forms.IntegerField(label=_("Informe o código recebido em seu email"), widget=forms.TextInput(attrs={'tabindex': 1, 'class': 'form-control'}))
    _user = None

    def __init__(self, *args, **kwargs):
        self.hash = kwargs.pop('hash', None)
        super().__init__(*args,**kwargs)

    def clean(self):
        cleaned_data = self.cleaned_data
        try:
            if 'codigo' in self.cleaned_data:
                t = TwoFactoAuth.objects.get(hash=self.hash)
                if t.code != self.cleaned_data['codigo']:
                    self.add_error('codigo', 'Código inválido')
                elif t.data < timezone.now() - relativedelta(minutes=5):
                    self.add_error('codigo', 'Código expirado')
                else:
                    self._user = t.user
        except:
            self.add_error('codigo', _('Código não encontrado'))
        return cleaned_data

    @property
    def user(self):
        return self._user