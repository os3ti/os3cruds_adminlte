import os
from setuptools import setup

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='cruds_adminlte',
    version='1.40',
    python_requires=">=3, <4",
    packages=['cruds_adminlte'],
    include_package_data=True,
    install_requires=[
        "Django>=3",
        "django-autocomplete-light>=3.5.1",
        "django-crispy-forms>=1.9.2",
    ],

)
# git push -u origin master